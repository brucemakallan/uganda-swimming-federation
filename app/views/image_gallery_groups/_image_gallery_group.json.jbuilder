json.extract! image_gallery_group, :id, :title, :body, :created_at, :updated_at
json.url image_gallery_group_url(image_gallery_group, format: :json)
