json.extract! video_gallery_item, :id, :title, :body, :image_url, :video_url, :video_gallery_name, :created_at, :updated_at
json.url video_gallery_item_url(video_gallery_item, format: :json)
