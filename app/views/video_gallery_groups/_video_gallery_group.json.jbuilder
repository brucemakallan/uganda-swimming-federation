json.extract! video_gallery_group, :id, :title, :body, :created_at, :updated_at
json.url video_gallery_group_url(video_gallery_group, format: :json)
