json.extract! latest_news_article, :id, :title, :body, :image_url, :created_at, :updated_at
json.url latest_news_article_url(latest_news_article, format: :json)
