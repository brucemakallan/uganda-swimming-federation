json.extract! image_gallery_item, :id, :title, :body, :image_url, :image_gallery_name, :created_at, :updated_at
json.url image_gallery_item_url(image_gallery_item, format: :json)
