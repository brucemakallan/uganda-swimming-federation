class ContactsMailer < ApplicationMailer
	default from: "donotreply.ugandaswimming@gmail.com"
	layout 'mailer'

	def send_email(email)
	    @email = email
	    mail(to: "donotreply.ugandaswimming@gmail.com", subject: email.subject)
	end
end
