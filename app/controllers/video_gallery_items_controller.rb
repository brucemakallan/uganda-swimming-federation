class VideoGalleryItemsController < ApplicationController
  before_action :set_video_gallery_item, only: [:show, :edit, :update, :destroy]

  #petergate
  #access all: [:show, :index], user: {except: [:destroy]}, company_admin: :all
  access all: [:show, :index], user: {except: [:destroy, :new, :create, :update, :edit]}, site_admin: :all

  layout "scaffolds"

  # GET /video_gallery_items
  # GET /video_gallery_items.json
  def index
    @video_gallery_items = VideoGalleryItem.all
    @gallerygroups = VideoGalleryGroup.all
  end

  # GET /video_gallery_items/1
  # GET /video_gallery_items/1.json
  def show
  end

  # GET /video_gallery_items/new
  def new
    @video_gallery_item = VideoGalleryItem.new
    @gallerygroups = VideoGalleryGroup.all
  end

  # GET /video_gallery_items/1/edit
  def edit
    @gallerygroups = VideoGalleryGroup.all
  end

  # POST /video_gallery_items
  # POST /video_gallery_items.json
  def create
    @video_gallery_item = VideoGalleryItem.new(video_gallery_item_params)

    respond_to do |format|
      if @video_gallery_item.save
        format.html { redirect_to video_gallery_items_url, notice: 'Video gallery item was successfully created.' }
        format.json { render :show, status: :created, location: @video_gallery_item }
      else
        format.html { render :new }
        format.json { render json: @video_gallery_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /video_gallery_items/1
  # PATCH/PUT /video_gallery_items/1.json
  def update
    respond_to do |format|
      if @video_gallery_item.update(video_gallery_item_params)
        format.html { redirect_to video_gallery_items_url, notice: 'Video gallery item was successfully updated.' }
        format.json { render :show, status: :ok, location: @video_gallery_item }
      else
        format.html { render :edit }
        format.json { render json: @video_gallery_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /video_gallery_items/1
  # DELETE /video_gallery_items/1.json
  def destroy
    @video_gallery_item.destroy
    respond_to do |format|
      format.html { redirect_to video_gallery_items_url, notice: 'Video gallery item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_video_gallery_item
      @video_gallery_item = VideoGalleryItem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def video_gallery_item_params
      params.require(:video_gallery_item).permit(:title, :body, :image_url, :video_url, :video_gallery_name)
    end
end
