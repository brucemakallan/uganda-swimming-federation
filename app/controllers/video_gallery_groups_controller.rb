class VideoGalleryGroupsController < ApplicationController
  before_action :set_video_gallery_group, only: [:show, :edit, :update, :destroy]
  #access all: [:index, :show, :new, :edit, :create, :update, :destroy], user: :all

  #petergate
  #access all: [:show, :index], user: {except: [:destroy]}, company_admin: :all
  access all: [:show, :index], user: {except: [:destroy, :new, :create, :update, :edit]}, site_admin: :all

  layout "scaffolds"

  # GET /video_gallery_groups
  def index
    @video_gallery_groups = VideoGalleryGroup.all
  end

  # GET /video_gallery_groups/1
  def show
  end

  # GET /video_gallery_groups/new
  def new
    @video_gallery_group = VideoGalleryGroup.new
  end

  # GET /video_gallery_groups/1/edit
  def edit
  end

  # POST /video_gallery_groups
  def create
    @video_gallery_group = VideoGalleryGroup.new(video_gallery_group_params)

    if @video_gallery_group.save
      redirect_to video_gallery_groups_url, notice: 'Video gallery group was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /video_gallery_groups/1
  def update
    if @video_gallery_group.update(video_gallery_group_params)
      redirect_to video_gallery_groups_url, notice: 'Video gallery group was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /video_gallery_groups/1
  def destroy
    @video_gallery_group.destroy
    redirect_to video_gallery_groups_url, notice: 'Video gallery group was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_video_gallery_group
      @video_gallery_group = VideoGalleryGroup.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def video_gallery_group_params
      params.require(:video_gallery_group).permit(:title, :body)
    end
end
