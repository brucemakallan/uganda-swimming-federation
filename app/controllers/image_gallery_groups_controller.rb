class ImageGalleryGroupsController < ApplicationController
  before_action :set_image_gallery_group, only: [:show, :edit, :update, :destroy]
  #access all: [:index, :show, :new, :edit, :create, :update, :destroy], user: :all

  #petergate
  #access all: [:show, :index], user: {except: [:destroy]}, company_admin: :all
  access all: [:show, :index], user: {except: [:destroy, :new, :create, :update, :edit]}, site_admin: :all

  layout "scaffolds"

  # GET /image_gallery_groups
  def index
    @image_gallery_groups = ImageGalleryGroup.all
  end

  # GET /image_gallery_groups/1
  def show
  end

  # GET /image_gallery_groups/new
  def new
    @image_gallery_group = ImageGalleryGroup.new
  end

  # GET /image_gallery_groups/1/edit
  def edit
  end

  # POST /image_gallery_groups
  def create
    @image_gallery_group = ImageGalleryGroup.new(image_gallery_group_params)

    if @image_gallery_group.save
      redirect_to image_gallery_groups_url, notice: 'Image gallery group was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /image_gallery_groups/1
  def update
    if @image_gallery_group.update(image_gallery_group_params)
      redirect_to image_gallery_groups_url, notice: 'Image gallery group was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /image_gallery_groups/1
  def destroy
    @image_gallery_group.destroy
    redirect_to image_gallery_groups_url, notice: 'Image gallery group was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_image_gallery_group
      @image_gallery_group = ImageGalleryGroup.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def image_gallery_group_params
      params.require(:image_gallery_group).permit(:title, :body)
    end
end
