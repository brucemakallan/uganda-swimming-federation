class PagesController < ApplicationController

  def home
    @events_blogs = Blog.order(created_at: :desc)
    # @events_blogs = Blog.order(:updated_at).page params[:page]
    @image_gallery_items = ImageGalleryItem.order(created_at: :desc)
    @latest_news_article = LatestNewsArticle.last
    @events = Event.order(event_date: :desc)
  end

  def events
    @page_title = "USF | Events"
    @events_blogs = Blog.order(created_at: :desc)
    @events = Event.order(event_date: :desc)
  end

  def results
    @page_title = "USF | Results"
    likestr = 'Results: '
    @results_blog_articles = Blog.order(created_at: :desc).where('title LIKE ?', "%#{likestr}%")
    @events = Event.order(event_date: :desc)
  end

  def calendars
    @page_title = "USF | Calendars"
  end

  def members
    @page_title = "USF | Members"
  end

  def athletes
    @page_title = "USF | Athletes"
  end

  def about
    @page_title = "USF | About"
  end

  def contact
    @page_title = "USF | Contact"
    if params[:commit] == "Send"
        puts "Send button Clicked"
        p "*" * 55

        fullname = params['fullname']
        email = params['email']
        subject = params['subject']
        message = params['message']

        #send email
        @email = Email.new(fullname: fullname, email: email, subject: subject, message: message)
        ContactsMailer.send_email(@email).deliver
        
        #redirect
        redirect_to home_path, notice: 'Your Message was sent'
    end
  end

  def gallery    
    @page_title = "USF | Image Gallery"
    @image_gallery_items = ImageGalleryItem.order(created_at: :desc)
    @image_gallery_groups = ImageGalleryGroup.all.order(created_at: :desc)
  end

  def gallery_group
    @image_gallery_items = ImageGalleryItem.order(created_at: :desc)
    @image_gallery_groups = ImageGalleryGroup.all.order(created_at: :desc)
  end
end
