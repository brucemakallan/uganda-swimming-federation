class ImageGalleryItemsController < ApplicationController
  before_action :set_image_gallery_item, only: [:show, :edit, :update, :destroy]

  #petergate
  #access all: [:show, :index], user: {except: [:destroy]}, company_admin: :all
  access all: [:show, :index], user: {except: [:destroy, :new, :create, :update, :edit]}, site_admin: :all

  layout "scaffolds"

  # GET /image_gallery_items
  # GET /image_gallery_items.json
  def index
    @image_gallery_items = ImageGalleryItem.all
    @gallerygroups = ImageGalleryGroup.all
  end

  # GET /image_gallery_items/1
  # GET /image_gallery_items/1.json
  def show
  end

  # GET /image_gallery_items/new
  def new
    @image_gallery_item = ImageGalleryItem.new
    @gallerygroups = ImageGalleryGroup.all
  end

  # GET /image_gallery_items/1/edit
  def edit
    @gallerygroups = ImageGalleryGroup.all
  end

  # POST /image_gallery_items
  # POST /image_gallery_items.json
  def create
    @image_gallery_item = ImageGalleryItem.new(image_gallery_item_params)

    respond_to do |format|
      if @image_gallery_item.save
        format.html { redirect_to image_gallery_items_url, notice: 'Image gallery item was successfully created.' }
        format.json { render :show, status: :created, location: @image_gallery_item }
      else
        format.html { render :new }
        format.json { render json: @image_gallery_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /image_gallery_items/1
  # PATCH/PUT /image_gallery_items/1.json
  def update
    respond_to do |format|
      if @image_gallery_item.update(image_gallery_item_params)
        format.html { redirect_to image_gallery_items_url, notice: 'Image gallery item was successfully updated.' }
        format.json { render :show, status: :ok, location: @image_gallery_item }
      else
        format.html { render :edit }
        format.json { render json: @image_gallery_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /image_gallery_items/1
  # DELETE /image_gallery_items/1.json
  def destroy
    @image_gallery_item.destroy
    respond_to do |format|
      format.html { redirect_to image_gallery_items_url, notice: 'Image gallery item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_image_gallery_item
      @image_gallery_item = ImageGalleryItem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def image_gallery_item_params
      params.require(:image_gallery_item).permit(:title, :body, :image_url, :image_gallery_name)
    end
end
