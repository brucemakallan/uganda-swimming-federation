class ApplicationController < ActionController::Base
	protect_from_forgery prepend: true

	before_action :set_title

	def set_title 
		@page_title = "The Uganda Swimming Federation"
		@events = Event.order(event_date: :desc)
	end

end
