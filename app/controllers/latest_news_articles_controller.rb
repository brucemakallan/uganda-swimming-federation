class LatestNewsArticlesController < ApplicationController
  before_action :set_latest_news_article, only: [:show, :edit, :update, :destroy]
  #access all: [:index, :show, :new, :edit, :create, :update, :destroy], user: :all

  #petergate
  #access all: [:show, :index], user: {except: [:destroy]}, company_admin: :all
  access all: [:show, :index], user: {except: [:destroy, :new, :create, :update, :edit]}, site_admin: :all

  layout "scaffolds"

  # GET /latest_news_articles
  def index
    @latest_news_articles = LatestNewsArticle.all
  end

  # GET /latest_news_articles/1
  def show
  end

  # GET /latest_news_articles/new
  def new
    @latest_news_article = LatestNewsArticle.new
  end

  # GET /latest_news_articles/1/edit
  def edit
  end

  # POST /latest_news_articles
  def create
    @latest_news_article = LatestNewsArticle.new(latest_news_article_params)

    if @latest_news_article.save
      redirect_to latest_news_articles_url, notice: 'Latest news article was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /latest_news_articles/1
  def update
    if @latest_news_article.update(latest_news_article_params)
      redirect_to latest_news_articles_url, notice: 'Latest news article was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /latest_news_articles/1
  def destroy
    @latest_news_article.destroy
    redirect_to latest_news_articles_url, notice: 'Latest news article was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_latest_news_article
      @latest_news_article = LatestNewsArticle.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def latest_news_article_params
      params.require(:latest_news_article).permit(:title, :body, :image_url)
    end
end
