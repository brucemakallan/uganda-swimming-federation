Rails.application.routes.draw do
  resources :latest_news_articles
  resources :video_gallery_groups
  resources :image_gallery_groups
  devise_for :users, path: '', path_names: {sign_in: 'login', sign_out: 'logout', sign_up: 'register_hidden'}
  resources :events
  resources :image_gallery_items
  resources :video_gallery_items
  resources :blogs
  get 'home', to: 'pages#home'
  get 'allevents', to: 'pages#events'
  get 'results', to: 'pages#results'
  get 'calendars', to: 'pages#calendars'
  get 'members', to: 'pages#members'
  get 'athletes', to: 'pages#athletes'
  get 'about', to: 'pages#about'
  get 'contact', to: 'pages#contact'
  post 'contact', to: 'pages#contact'
  get 'gallery', to: 'pages#gallery'
  get 'gallery_group', to: 'pages#gallery_group'

  root to: 'pages#home'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
