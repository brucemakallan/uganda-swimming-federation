class CreateImageGalleryItems < ActiveRecord::Migration[5.1]
  def change
    create_table :image_gallery_items do |t|
      t.string :title
      t.text :body
      t.string :image_url
      t.string :image_gallery_name

      t.timestamps
    end
  end
end
