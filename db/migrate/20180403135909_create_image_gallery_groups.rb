class CreateImageGalleryGroups < ActiveRecord::Migration[5.1]
  def change
    create_table :image_gallery_groups do |t|
      t.string :title
      t.text :body

      t.timestamps
    end
  end
end
