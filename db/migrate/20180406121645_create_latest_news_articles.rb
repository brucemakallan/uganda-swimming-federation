class CreateLatestNewsArticles < ActiveRecord::Migration[5.1]
  def change
    create_table :latest_news_articles do |t|
      t.string :title
      t.text :body
      t.string :image_url

      t.timestamps
    end
  end
end
