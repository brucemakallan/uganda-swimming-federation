class CreateVideoGalleryItems < ActiveRecord::Migration[5.1]
  def change
    create_table :video_gallery_items do |t|
      t.string :title
      t.text :body
      t.string :image_url
      t.string :video_url
      t.string :video_gallery_name

      t.timestamps
    end
  end
end
